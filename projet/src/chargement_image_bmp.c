/********************************************************
*                                                       *
* Permet de charger une image sous forme de tableau de  *
* float pour être utilisé comme texture par openGL      *
*                                                       *
*********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../inc/chargement_image_bmp.h"
#pragma pack(1) // désactive l'alignement des pixels pour pas que gcc ajout des bits dans les structures créées.

/* -- définition de structures pour contenir l'entête du fichier bmp -- */

static struct en_tete_image_bmp {
	int size_imhead;

	int largeur;
	int longueur;

	short nbplans; // toujours 1
	short bpp;
  // méthode de compression
	int compression;
	int sizeim;

  // utilisé pour l'impression, ne sera pas utilisé par la suite
	int resolution_largeur;
	int resolution_longueur;

  // palette de couleur, en 24bits il y en a pas car chaque pixel(3octets) correspond à une couleur.
	int cpalette;
	int cIpalette;
};

static struct en_tete_fichier_bmp {
	char signature[2];
	int taille;
	int rsv;
	int offsetim;
	struct en_tete_image_bmp en_tete_image_bmp;
};

/****
*  charche et renvoie une image à partir du fichier
*  dont le chemin est passé en paramètre
* Renvoie un pointeur sur l'image ou NULL.
****/
image *bmp_chargement(char *chemin) {
  struct en_tete_fichier_bmp en_tete; // celle du fichier lu
  image *image; // l'image créée
  int i,j; // pour la lecture de l'image
  unsigned char pixel_lu[3];
  FILE *fichier;
  int bits_ajoute_par_ligne; // une ligne doit être un multiple de 4
  char nbbits_ajoute[4] = {0, 3, 2, 1}; /* les lignes sont multiples de 4,
                                           donc il peut y avoir besoin de compléter */

  /* -- ouverture du fichier -- */
  fichier = fopen(chemin, "rb"); // ouverture en lecture binaire du fichier
  if(fichier == NULL)
  {
      fprintf(stderr, "Erreur d'ouverture de l'image de la texture : %s\n", chemin);
      return NULL;
  }

  /* -- lecture et analyse de l'en-tête du fichier -- */
  fread(&en_tete, sizeof(struct en_tete_fichier_bmp), 1, fichier);
  // https://www.commentcamarche.net/contents/1200-bmp-format-bmp
  if (en_tete.signature[0] != 'B'  ||  en_tete.signature[1] != 'M')
  {
    fprintf(stderr, "Erreur de signature, BMP non supporté.\n");
    return NULL;
  }
	if (en_tete.en_tete_image_bmp.bpp != 24)
  {
    fprintf(stderr, "Erreur, que les images avec couleur 24 bits sont utilisées (un octet par couleur).\n");
    return NULL;
  }
	if (en_tete.en_tete_image_bmp.compression != 0)
  {
    fprintf(stderr, "Erreur, le fichier bmp est compressé.\n");
    return NULL;
  }
	if (en_tete.en_tete_image_bmp.cpalette != 0  ||  en_tete.en_tete_image_bmp.cIpalette != 0)
  {
    fprintf(stderr, "Erreur, les palettes ne sont pas supportées.\n");
    return NULL;
  }


  /* -- réservation de l'espace mémoire nécessaire à l'image -- */
  image = malloc(sizeof(image));
  if(image == NULL)
  {
    fprintf(stderr, "Erreur de malloc pour la création de l'image %s", chemin);
    fclose(fichier);
    return NULL;
  }

  image->largeur = en_tete.en_tete_image_bmp.largeur;
  image->longueur = en_tete.en_tete_image_bmp.longueur;

  /*
   détermination du pas, suivant la taille de chaque ligne du fichier :
     - (3*X)%4 == 0, pas de bits à rajouter
     - (3*X)%4 == 1, compléter avec 3 octets,
     - (3*X)%4 == 2, compléter avec 2 octets,
     - (3*X)%4 == 3, compléter avec 1 octets.
  */
  bits_ajoute_par_ligne = nbbits_ajoute[(3*en_tete.en_tete_image_bmp.largeur)%4];

  /* -- lecture ligne à ligne du fichier --*/
  for(j=0; j<image->longueur; j++)
  {
  	for(i=0; i<image->largeur; i++)
  	{
  		fread(&pixel_lu, 1, 3, fichier);
      // * 3 car un pixel composé de 3 octets RGB
  		image->donnees[image->largeur * 3 * j + i * 3] = pixel_lu[0];
  		image->donnees[image->largeur * 3 * j + i * 3 + 1] = pixel_lu[1];
  		image->donnees[image->largeur * 3 * j + i * 3 + 2] = pixel_lu[2];
  	}

    /* -- lecture des bits ajoutés pour compléter la ligne -- */
  	fread(&pixel_lu, 1, bits_ajoute_par_ligne, fichier);
  }

  fclose(fichier);
  return image;
}
