/**
* Ce module permet de créer les différents objets
* composant l'environnement 3D du projet :
*   - maison
*   - ...
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/gl.h"
#include "GL/glut.h"
#include "../inc/8-arbre.h"
#include "../inc/couleur.h"
#include "../inc/matrice.h"
#include "../inc/objet.h"
#include "../inc/parametres.h"

 int dessiner_cube_englobant = 0;

/* ***
* dessine un cube plein (plein = 1) ou vide (plein = 0).
*** */
void dessiner_cube(cube cube, int plein, float couleur[3]) {

  if(plein) { // dessine des carrés plein
    glBegin(GL_QUADS);

    glColor3fv(couleur);

    // face avant :
    glVertex3fv(cube.p1);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);

    // face arrière
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);
    glVertex3fv(cube.p2);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);


    // face haut
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);
    glVertex3fv(cube.p2);
    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);

    // face bas
    glVertex3fv(cube.p1);
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);

    // face gauche
    glVertex3fv(cube.p1);
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);

    // face droite
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);
    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);
    glVertex3fv(cube.p2);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);

  }
  else { // dessine les aretes
    glBegin(GL_LINES);

    // augmente l'épaisseur des ligne pour une meilleur visibilité
    glLineWidth(2.5);

    glColor3fv(couleur);

    // face avant :
    glVertex3fv(cube.p1);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);

    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);

    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);

    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);
    glVertex3fv(cube.p1);


    // face arrière
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);

    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);
    glVertex3fv(cube.p2);

    glVertex3fv(cube.p2);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);

    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);


    // face haut
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);
    glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);

    //glVertex3f(cube.p1[X], cube.p2[Y], cube.p2[Z]);
    //glVertex3fv(cube.p2);

    glVertex3fv(cube.p2);
    glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);

    //glVertex3f(cube.p2[X], cube.p2[Y], cube.p1[Z]);
    //glVertex3f(cube.p1[X], cube.p2[Y], cube.p1[Z]);


    // face bas
    glVertex3fv(cube.p1);
    glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);

    //glVertex3f(cube.p1[X], cube.p1[Y], cube.p2[Z]);
    //glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);

    glVertex3f(cube.p2[X], cube.p1[Y], cube.p2[Z]);
    glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);

    //glVertex3f(cube.p2[X], cube.p1[Y], cube.p1[Z]);
    //glVertex3fv(cube.p1);

    // aretes des faces gauches et droites déjà dessinées.

    // réinitialise l'épaisseur
    glLineWidth(1);
  }

  glEnd();
}


/* *** dessine la maison donnéee en paramètre *** */
void dessiner_maison(maison maison) {

  if(dessiner_cube_englobant){
    dessiner_cube(maison.cube_englobant, 0, (float[]){COULEUR_PAR_DEFAUT});
  }

  /* -- dessin des murs de la maison */
  cube murs_maison = {
    {
      maison.cube_englobant.p1[X],
      maison.cube_englobant.p1[Y],
      maison.cube_englobant.p1[Z]
    },
    {
      maison.cube_englobant.p1[X]+maison.hauteur_mur,
      maison.cube_englobant.p1[Y]+maison.hauteur_mur,
      maison.cube_englobant.p1[Z]-maison.hauteur_mur
    },
    maison.hauteur_mur
  };
  dessiner_cube(murs_maison, 1, (float[]){COULEUR_MUR});

  /* -- dessin du toit -- */
  // cacul des mesures du toit
  float cote_pentu = (maison.hauteur_mur / 2.f) / sin(maison.pente_toit);
  float hauteur_toit = cos(maison.pente_toit) * cote_pentu;
  if(hauteur_toit < 0) hauteur_toit *= -1.f;
  hauteur_toit += murs_maison.p2[Y];
  float millieu_toit = murs_maison.p1[X] + maison.hauteur_mur/2.f;

  // dessin
  glBegin(GL_TRIANGLES);
  glColor3f(COULEUR_TOIT);
  // face avant
  glVertex3f(murs_maison.p1[X], murs_maison.p2[Y], murs_maison.p1[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p1[Z]);
  glVertex3f(murs_maison.p2[X], murs_maison.p2[Y], murs_maison.p1[Z]);

  // face arrière
  glVertex3f(murs_maison.p1[X], murs_maison.p2[Y], murs_maison.p2[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p2[Z]);
  glVertex3fv(murs_maison.p2);

  glEnd();

  glBegin(GL_QUADS);
  // face gauches
  glVertex3f(murs_maison.p1[X], murs_maison.p2[Y], murs_maison.p1[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p1[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p2[Z]);
  glVertex3f(murs_maison.p1[X], murs_maison.p2[Y], murs_maison.p2[Z]);

  // face droite
  glVertex3f(murs_maison.p2[X], murs_maison.p2[Y], murs_maison.p1[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p1[Z]);
  glVertex3f(millieu_toit, hauteur_toit, murs_maison.p2[Z]);
  glVertex3f(murs_maison.p2[X], murs_maison.p2[Y], murs_maison.p2[Z]);


  glEnd();

}


/* -- permet de distinguer la maison cible des autres maisons -- */
void dessiner_distinction_maison_cible() {
  glBegin(GL_QUADS);
  glColor3f(COULEUR_CIBLE);

  glVertex3f(
    maison_cible.cube_englobant.p1[X],
    maison_cible.cube_englobant.p1[Y],
    maison_cible.cube_englobant.p1[Z] + 1
  );
  glVertex3f(
    maison_cible.cube_englobant.p1[X],
    maison_cible.cube_englobant.p1[Y] + maison_cible.hauteur_mur,
    maison_cible.cube_englobant.p1[Z] + 1
  );
  glVertex3f(
    maison_cible.cube_englobant.p1[X] + maison_cible.hauteur_mur,
    maison_cible.cube_englobant.p1[Y] + maison_cible.hauteur_mur,
    maison_cible.cube_englobant.p1[Z] + 1
  );
  glVertex3f(
    maison_cible.cube_englobant.p1[X] + maison_cible.hauteur_mur,
    maison_cible.cube_englobant.p1[Y],
    maison_cible.cube_englobant.p1[Z] + 1
   );

  glEnd();
}




/****
* Créer une maison à la position (x, y, z) avec des paramètres aléatoires.
****/
maison creer_maison_alea(float x, float y, float z) {
  maison maison;
  int tmps;
  float cote;

  maison.type = MAISON;

  // définition du cube englobant
  cote = rand() % 100 + 20;
  maison.cube_englobant = (cube) {
    {x, y, z},                /* p1   */
    {x+cote, y+cote, z-cote}, /* p2   */
    cote
  };
  tmps = 0.25 * cote;
  maison.hauteur_mur = rand() % tmps + 0.50f * cote;
  maison.pente_toit = rand() % 10 + 30.f;

  return maison;
}
