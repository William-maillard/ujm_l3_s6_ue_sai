/***
* Fonctions permettant de créer, manipuler, afficher un octree.
****/
#include <stdio.h>
#include <stdlib.h>
//#include "GL/gl.h"
//#include "GL/glut.h"
#include "../inc/8-arbre.h"
#include "../inc/camera.h"
#include "../inc/couleur.h"
#include "../inc/jeu.h"
#include "../inc/matrice.h"
#include "../inc/objet.h"
#include "../inc/parametres.h"

/****
*
****/
int affichage_abre = 0;
arbre arbre_monde;
maison maison_cible;

/****
* Creer un noeud  avec la position de la subdivision du cube et sa taille
* passé en paramètre.
****/
arbre creer_noeud(float position_centre[3], int taille_cote) {
  arbre noeud = (arbre) malloc(sizeof(struct struct_noeud));

  if(noeud == NULL) {
    fprintf(stderr, "Erreur de malloc à la création du noeud.\n");
    return NULL;
  }

  noeud->position_centre[X] = position_centre[X];
  noeud->position_centre[Y] = position_centre[Y];
  noeud->position_centre[Z] = position_centre[Z];
  noeud->taille_cote = taille_cote;
  noeud->fils = NULL;
  noeud->nb_objets = 0;
  noeud->objets = (objet*) malloc(sizeof(union u_objet)*NOMBRE_MAX_OBJETS_PAR_CUBE);


  if(noeud->objets == NULL)
  {
    fprintf(stderr, "Erreur malloc création tableau d'objets.\n");
    return NULL;
  }

  return noeud;
}

/****
* renvoie 1 si le noeud est une feuille,
* 0 sinon.
****/
int est_feuille(arbre noeud){
  return (noeud->fils == NULL) ? 1 : 0;
}

/* -- choisie des coordonées aléatires dans le monde -- */
static void coord_alea(float *x, float *y, float *z) {
  *x = rand() % WORLD_WIDTH - WORLD_BOUND_X;
  *y = rand() % WORLD_BOUND_Y;
  *z = rand() % WORLD_DEPTH  - WORLD_BOUND_Z;
}


/****
* Initialise l'arbre du programme avec 1 racine et 8 feuilles.
****/
void initialiser_arbre(int n) {
  int i;            /* index                                        */
  float x, y, z;    /* pour tirer une position aléatoire            */
  objet objet;      /* l'objet créé                                 */


  printf("Init.. ");
    arbre_monde = creer_noeud(
      (float[3]){0.f, 0.f, 0.f},
      WORLD_COTE_CUBE
    );
    subdiviser_arbre(arbre_monde);
    printf("arbre initialisé.\n");

    printf("Création et placement des objets...\n");

    /* -- création de la maison cible -- */
    coord_alea(&x, &y, &z);
    maison_cible = creer_maison_alea(x, y, z);
    objet.maison = maison_cible;
    inserer_objet(arbre_monde, objet);
    init_distance_precedente();
    printf("maison cible : (%f, %f, %f), distance : %f.\n", x, y, z, distance_precedente);


    for(i=0; i<n; i++)
    {
      coord_alea(&x, &y, &z);

      // tirage aléatoire du type d'objet.

      objet.maison = creer_maison_alea(x, y, z);

      inserer_objet(arbre_monde, objet);
    }
    printf("fait.\n");


}


/****
* permet de trouver l'index de la subdivision du cube auquel appartient
* le vecteur.
* Renvoie cet index.
****/
int trouver_index_subdivision(float position[3], float centre_subdivision[3]) {
  int index = 0;

  index |= position[Y] > centre_subdivision[Y] ? A_HAUT : A_BAS;
  index |= position[X] > centre_subdivision[X] ? A_DROITE : A_GAUCHE;
  index |= position[Z] > centre_subdivision[Z] ? A_AVANT : A_ARRIERE ;//: A_AVANT;

  return index;
}


/****
* Divise en 8 subdivisions de l'espace le noeud passé en paramètre.
* Renvoie 1 si le noeud a été subdiviser,
* 0 sin on ne peut plus le subdiviser.
****/
int subdiviser_arbre(arbre noeud){
  float position[3];
  int i;
  float cote_subdivision = noeud->taille_cote/ 2.f;
  float quart_taille_cote = noeud->taille_cote / 4.f;

  if(quart_taille_cote <= 0)
   {
    fprintf(stderr, "Erreur subdivision du noeud impossible, taille minimale atteinte");
    return 0;
  }

  /* -- réservation de l'espace mémoire -- */
  noeud->fils = (arbre *) malloc(8*sizeof(arbre));

  if(noeud->fils == NULL)
  {
    fprintf(stderr, "Erreur de malloc à la création des fils.\n");
    return 0;
  }

  /* -- création des fils -- */
  //printf("centre noeud courant (%f, %f, %f), cote %f\n", noeud->position_centre[X], noeud->position_centre[Y], noeud->position_centre[Z], noeud->taille_cote);
  for(i=0; i<8; i++)
  {
    //printf("%d : ", i);
    /* -- reinitialise la position courante -- */
    position[X] = noeud->position_centre[X];
    position[Y] = noeud->position_centre[Y];
    position[Z] = noeud->position_centre[Z];

    /* -- calcule le centre de la subdivision courante -- */
    /* on vérifie si l'entier i contient les bits représentant HAUT/DROIT/BAS
       et on en deduis dans quel sens décaler la position par rapport au
       centre du cube contenant la subdivision.*/
    position[Y] += ((i&A_HAUT) == A_HAUT) ? quart_taille_cote : -quart_taille_cote;
    position[X] += ((i&A_DROITE) == A_DROITE) ? quart_taille_cote : -quart_taille_cote;
    position[Z] += ((i&A_ARRIERE) == A_ARRIERE) ? -quart_taille_cote : quart_taille_cote;

    /* version print debug
    if((i&A_HAUT) == A_HAUT) {
      position[Y] +=  quart_taille_cote;
      printf("HAUT ");
    }
    else{
      position[Y] -= quart_taille_cote;
       printf("BAS ");
    }
    if((i&A_DROITE) == A_DROITE) {
       position[X] += quart_taille_cote;
        printf("DROITE ");
    }
    else{
      position[X] -= quart_taille_cote;
       printf("GAUCHE ");
    }
    if((i&A_ARRIERE) == A_ARRIERE) {
      position[Z] += quart_taille_cote;
       printf("ARRIERE ");
    } else{
      position[Z] -= quart_taille_cote;
       printf("AVANT ");
    }
    */

    /* -- création du noeud -- */
    noeud->fils[i] = creer_noeud(position, cote_subdivision);
    //printf(" centre noeud (%f, %f, %f)\n", position[X], position[Y], position[Z]);
  }
  //printf("\n\n------\n");

  /* -- il faut redistribuer les objets du noeud dans les feuilles -- */
  for(i=0; i<noeud->nb_objets; i++) {
    inserer_objet(noeud, noeud->objets[i]);
  }
  free(noeud->objets);
  noeud->objets = NULL;

  return 1;
}



int inserer_objet(arbre arbre, objet objet) {
  float centre_objet[3], p1[3], p2[3];
  int index_subdivision;

  // cas d'arrêt de la récursivité.
  if(est_feuille(arbre)){
    // regarder si reste de la place, si oui ajouter, sinon subdiviser.
    if(arbre->nb_objets < NOMBRE_MAX_OBJETS_PAR_CUBE) {
      arbre->objets[arbre->nb_objets] = objet;
      arbre->nb_objets++;
      return 1;
    }

    //printf("Noeud plein, on le subdivise.\n");
    subdiviser_arbre(arbre);
    if(arbre->fils == NULL) {
      //printf("Subdivision impossible, arbre saturé.\n");
      return 0;
    }

    /* -- on l'insère dans le noeud courant, en sortant du if -- */
  }

  /* -- on recupere les points min/max de son cube englobant -- */
  if(objet.type == MAISON){
    p1[X] = objet.maison.cube_englobant.p1[X];
    p1[Y] = objet.maison.cube_englobant.p1[Y];
    p1[Z] = objet.maison.cube_englobant.p1[Z];
    p2[X] = objet.maison.cube_englobant.p2[X];
    p2[Y] = objet.maison.cube_englobant.p2[Y];
    p2[Z] = objet.maison.cube_englobant.p2[Z];
  }
  else{
    fprintf(stderr, "Erreur, type inconu dans 8-arbre.c pour l'insertion.\n");
    return 0;
  }

  /* -- on cherche dans quelle zone il faut l'insérer récursivement -- */
  centre_objet[X] = p1[X] + p2[X] - p1[X];
  centre_objet[Y] = p1[Y] + p2[Y] - p1[Y];
  centre_objet[Z] = p1[Z] + p2[Z] - p1[Z];

  index_subdivision =
   trouver_index_subdivision(centre_objet, arbre->position_centre);

   /* -- on l'insère -- */
   return inserer_objet(arbre->fils[index_subdivision], objet);
}


/*****
* Dessine les objets contenu dans le 8-arbre.
* Si la variable affichage_arbre = 1 alors
* dessine le 8-arbre en fil de fer.
****/
void dessiner_objets(arbre arbre) {
  int i;
  float tmps = arbre->taille_cote / 2.f;
  cube cube;

  /* -- est-on dans les limites de la fenetre ? -- */
  if(
    (arbre->position_centre[X] + tmps) < bordure_fenetre[F_GAUCHE] ||
    (arbre->position_centre[X] - tmps) > bordure_fenetre[F_DROIT] ||
    (arbre->position_centre[Y] + tmps) < bordure_fenetre[F_BAS] ||
    (arbre->position_centre[Y] - tmps) > bordure_fenetre[F_HAUT] ||
    (arbre->position_centre[Z] - tmps) > (position_point_de_vue[Z] - AVANT) ||
    (arbre->position_centre[Z] + tmps) < (position_point_de_vue[Z] - ARRIERE - AVANT)
  )
  ;//  return;

  if(est_feuille(arbre))
  {
     /* -- on dessine les objets -- */
     for(i=0; i<arbre->nb_objets; i++)
     {
       if(arbre->objets[i].type == MAISON)
       {
         dessiner_maison(arbre->objets[i].maison);
       }
       else{
         fprintf(stderr, "Erreur objet inconu dans l'arbre.\n");
       }
     }

     /* -- affichage de la subdivision ? -- */
     if(affichage_abre)
     {
       i = arbre->taille_cote / 2;

       cube.cote = arbre->taille_cote;

       cube.p1[X] = arbre->position_centre[X] - i;
       cube.p1[Y] = arbre->position_centre[Y] - i;
       cube.p1[Z] = arbre->position_centre[Z] - i;

       cube.p2[X] = arbre->position_centre[X] + i;
       cube.p2[Y] = arbre->position_centre[Y] + i;
       cube.p2[Z] = arbre->position_centre[Z] + i;

       dessiner_cube(cube, 0, (float[3]){COULEUR_PAR_DEFAUT});
     }
  }
  else // on dessine les subdivisions
  {
    for(i=0; i<8; i++)
    {
      dessiner_objets(arbre->fils[i]);
    }
  }
}
