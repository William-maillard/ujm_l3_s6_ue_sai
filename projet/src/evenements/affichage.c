/**
* Fonction pour les évènements d'affichage.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "GL/gl.h"
#include "GL/glut.h"

#include "../../inc/8-arbre.h"
#include "../../inc/camera.h"
#include "../../inc/couleur.h"
#include "../../inc/evenements.h"
#include "../../inc/jeu.h"
#include "../../inc/matrice.h"
#include "../../inc/objet.h"

#include "../../inc/parametres.h"

/* -- dimension de la fenetre -- */
float bordure_fenetre[4];

void afficher_sol();

/*** fonction d'affichage de la fenetre graphique ***/
void afficher_scene() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    /* -- Init matrice visualisation a la matrice identite -- */
    //glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* -- definition du point de vue de l'utilisateur -- */
    //glFrustum(GAUCHE, DROITE, BAS, HAUT, AVANT, ARRIERE);
    gluLookAt(
      position_point_de_vue[X], position_point_de_vue[Y], position_point_de_vue[Z],
      position_point_cible[X], position_point_cible[Y], position_point_cible[Z],
      camera_up[X], camera_up[Y], camera_up[Z]
    );

    afficher_sol();

    /*dessiner_cube(
      (cube) {
        {-50.f, 0.f, 100.0f},
        {50.f, 100.f, 0.f},
        100.f
      },
      0,
      (float[3]){COULEUR_PAR_DEFAUT}
    );*/
    /*
    cube cube_englobant = (cube) {
      {-100, 0, -30},
      {100, 200, -230},
      200
    };
    maison maison = {
      MAISON,
      cube_englobant,
      150, 45
    };
    dessiner_maison(maison);
    dessiner_cube(maison.cube_englobant, 0, (float[3]){COULEUR_PAR_DEFAUT});
    */


    dessiner_objets(arbre_monde);
    dessiner_distinction_maison_cible();


    if(afficher_indice) {
      /* glut ne supporte pas l'affichage de chiffre
      float x = position_point_de_vue[X] + bordure_fenetre[F_DROIT],
            y = position_point_de_vue[Y] + bordure_fenetre[F_HAUT],
            z = position_point_de_vue[Z] - AVANT;
      glPushMatrix();
      glLoadIdentity();

      glRasterPos3f(x - 20, y - 20, z);
      char s_distance[20];
      sprintf(s_distance, "%f", distance_precedente);
      int i, n = strlen(s_distance);

      for(i=0; i<n; i++)
      {
         glutBitmapCharacter(NULL, s_distance[i]);
      }

      glPopMatrix();
      */
      float x = position_point_de_vue[X] + bordure_fenetre[F_DROIT]/2.f,
            y = position_point_de_vue[Y] + bordure_fenetre[F_HAUT]/2.f,
            z = position_point_de_vue[Z] - AVANT;
      glBegin(GL_QUADS);
      glColor3fv(couleur);
      glVertex3f(x-20, y-20, z);
      glVertex3f(x-20, y-20, z);
      glVertex3f(x, y, z);
      glVertex3f(x, y, z);
      glEnd();

    }

    glutSwapBuffers();// envoie au buffer d'affichage
}



void afficher_sol() {
    glBegin(GL_QUADS);
    glColor3fv((float[]){COULEUR_HERBE});
    // angle avant gauche
    glVertex3i(-WORLD_BOUND_X, 0, -WORLD_BOUND_Z);
    // angle avant droite
    glVertex3i(WORLD_BOUND_X, 0, -WORLD_BOUND_Z);
    // angle arriere droite
    glVertex3i(WORLD_BOUND_X, 0, WORLD_BOUND_Z);
    // angle arriere gauche
    glVertex3i(-WORLD_BOUND_X, 0, WORLD_BOUND_Z);

    /*-- trace des bords pour voir le contour du monde
    // bord gauche
    glColor3f(1, 0, 0);
    glVertex3i(-WORLD_BOUND_X, 0, -WORLD_BOUND_Z);
    glVertex3i(-WORLD_BOUND_X, 0, WORLD_BOUND_Z);
    glVertex3i(-WORLD_BOUND_X, WORLD_BOUND_Y, WORLD_BOUND_Z);
    glVertex3i(-WORLD_BOUND_X, WORLD_BOUND_Y, -WORLD_BOUND_Z);
    */

    glEnd();
}


void redimensionner_fenetre(int largeur, int longueur) {
  float ratio;

  // pour se prémunir contre la division par 0
  if(largeur == 0) largeur++;
  if(longueur == 0) longueur++;

  /* -- mis à jour des limites de la fenetre -- */
  bordure_fenetre[F_HAUT] = position_point_de_vue[Y] + longueur;
  bordure_fenetre[F_DROIT] = position_point_de_vue[X] + longueur/2.f;
  bordure_fenetre[F_GAUCHE] = position_point_de_vue[X] - longueur/2.f;
  bordure_fenetre[F_BAS] = position_point_de_vue[Y];

  ratio = (float)largeur / (float)longueur;

  /* -- chargement et reinitialisation de la matrice de projection --*/
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  /* -- mise à jour du frustum -- */
  glViewport(0, 0, largeur, longueur);
  gluPerspective(ANGLE_DE_VU, ratio, AVANT, ARRIERE);

  /* -- replacement de la matrice de vue -- */
  glMatrixMode(GL_MODELVIEW);
}

void actualiser_animation() {
  static clock_t temps_precedent = 0;

  int gagne;
  clock_t temps_courant = clock();
  float temps_ecoule;

  temps_ecoule = (temps_courant - temps_precedent) / CLOCKS_PER_SEC;

  if(temps_ecoule >= TEMPS_RAFRAICHISSEMENT_INDICE)
  {
    gagne = maison_trouve();
    if(gagne) {;}

    temps_precedent = temps_courant;
  }
}



void fonction_periodique(int valeur) {

}
