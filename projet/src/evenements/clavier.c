/*
* Fonctions pour les évènements clavier.
*
*/
#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique

#include "../../inc/evenements.h"
#include "../../inc/camera.h"
#include "../../inc/parametres.h"

void traiter_entree_clavier(unsigned char key, int x, int y) {

  /* -- activer/désactiver l'affichage de l'arbre en fil de fer -- */
  if(key == 'a')
  {
    affichage_abre = (affichage_abre) ? 0 : 1;
  }

  /* -- activer/désactiver l'affichage des boîtes englobantes -- */
  else if(key == 'b')
  {
    dessiner_cube_englobant = (dessiner_cube_englobant) ? 0 : 1;
  }

  /* -- afficher la distance jusqu'à la maison cible -- */
  else if(key == 'i')
  {
    afficher_indice = (afficher_indice) ? 0 : 1;
  }


  /* -- affichage des changements -- */
  glutPostRedisplay();
}

/* pour traiter les caractères non ASCII */  /**
    GLUT_KEY_F1 F2 ...
    GLUT_KEY_LEFT
    GLUT_KEY_UP
    DGLUT_KEY_RIGHT
    GLUT_KEY_DOWN
    GLUT_KEY_PAGE_DOWN
    GLUT_KEY_PAGE_DOWN
    GLUT_KEY_HOME
    GLUT_KEY_END
    GLUT_KEY_INSERT
    */
void traiter_entree_clavier_special(int key, int x, int y) {
  switch(key)
  {
    case GLUT_KEY_LEFT:
      deplacer_camera_gauche();
      break;

    case GLUT_KEY_UP:
      avancer_camera();
      break;

    case GLUT_KEY_DOWN:
      reculer_camera();
      break;

    case GLUT_KEY_RIGHT:
      deplacer_camera_droite();
      break;
  }

  glutPostRedisplay();
}






















// end of file
