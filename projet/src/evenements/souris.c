/*
* Fonctions pour les évènements souris.
*
*/
#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique

#include "../../inc/evenements.h"
#include "../../inc/camera.h"

int x_souris, y_souris;
int deplacer_camera;

/*
bouton : GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
state : GLUT_UP, GLUT_DOWN
*/
void taiter_click_souris(int bouton, int etat, int x, int y) {

  if(bouton == GLUT_LEFT_BUTTON)
  {
    if(etat == GLUT_DOWN)
    {
      // sauvegarde de l'emplacement du click
      x_souris = x;
      y_souris = y;

      deplacer_camera = 1;

      // changement du curseur de la souris
      glutSetCursor(GLUT_CURSOR_INFO);
    }
    else
    {
      // réinitialisation
      glutSetCursor(0);
      deplacer_camera = 0;
    }
  }
}

// retourne la valeur absolue de x :
//static int val_absolue(int x) { return (x>0)? x : -x;}

void traiter_mouvement_souris_avec_bouton_click(int x, int y) {
  int delta_x;
  int delta_y;

  if(deplacer_camera)
  {
    /* -- calcul du déplacement relatif de la souris -- */
    delta_x =  x_souris - x;
    delta_y = y_souris - y;

    /* -- mise à jour de la position de la souris -- */
    x_souris = x;
    y_souris = y;

    orienter_camera(delta_x, delta_y);
    glutPostRedisplay();
  }
}


void traiter_mouvement_souris(int x, int y) {

}
