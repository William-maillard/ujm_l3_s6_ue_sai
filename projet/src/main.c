#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique

#include "../inc/8-arbre.h"
#include "../inc/evenements.h"
#include "../inc/camera.h"
#include "../inc/couleur.h"

void usage(char *s) {
    exit(0);
}

int main(int argc, char **argv) {

    /* -- Initialisation de GLUT -- */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);
    /* GLU_DEPTH est un buffer pour gérer les faces cachés */

    /* -- initialisations -- */
    initialiser_variables_camera();
    srand(getpid());

    /* -- Position et taille de la fenetre -- */
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    // glutFullScreen();

    /* -- Creation de la fenetre -- */
    glutCreateWindow("environnement 3D openGL");
    glClearColor(COULEUR_CIEL, 0.5f);


    /* -- inscriptions de fonctions de rappel pour les évènements -- */
    glutDisplayFunc(afficher_scene);
    glutReshapeFunc(redimensionner_fenetre);
    glutIdleFunc(actualiser_animation);
    //glutTimerFunc(TEMPS_RAFRAICHISSEMENT_INDICE, fonction_periodique, TEMPS_RAFRAICHISSEMENT_INDICE);

    glutKeyboardFunc(traiter_entree_clavier);
    glutSpecialFunc(traiter_entree_clavier_special);

    glutMouseFunc(taiter_click_souris);
    glutMotionFunc(traiter_mouvement_souris_avec_bouton_click);
    glutPassiveMotionFunc(traiter_mouvement_souris);

    // gluEntryFunc(void(*func) (etat int));

    glEnable(GL_DEPTH_TEST);
    /* -- Activation de la transparence -- */
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    affichage_abre = 1;

    if(argc == 1)
      initialiser_arbre(200);
    else
      initialiser_arbre(atoi(argv[1]));

    glutMainLoop();
    return 0;
}
