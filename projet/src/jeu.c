/***
* Fonctions permettant d'avoir un petit jeu de
* recherche de maison avec un indicateur pour savoir si on
* se rapproche ou non de l'objectif.
****/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../inc/8-arbre.h"
#include "../inc/camera.h"
#include "../inc/couleur.h"
#include "../inc/matrice.h"
#include "../inc/objet.h"
#include "../inc/parametres.h"




/* -- indice pour la maison à trouver -- */
int afficher_indice = 0;
float distance_precedente;
float couleur[3];


static float calcul_distance_camera_maison_cible() {
  return sqrt(
    pow(position_point_de_vue[X] - maison_cible.cube_englobant.p1[X], 2) +
    pow(position_point_de_vue[Y] - maison_cible.cube_englobant.p1[Y], 2) +
    pow(position_point_de_vue[Z] - maison_cible.cube_englobant.p1[Z], 2)
  );
}


/****
* Initialise la distance entre la camera et la maison cible
****/
void init_distance_precedente() {
  distance_precedente = calcul_distance_camera_maison_cible();
  couleur[0] = 0.5f;
  couleur[1] = 0.f;
  couleur[2] = 0.f;
}


/* *** Renvoie 1 si la maison a ete trouve, 0 sinon. *** */
int maison_trouve() {
  float distance_courante = calcul_distance_camera_maison_cible();
  float tmps = ((int)(distance_precedente -distance_courante) % 255) / 255.f;
  int couleur_affiche, couleur_suppr;

  if(distance_courante == distance_precedente) return 0;

  printf("distance : %f.\n", distance_courante);

  // on se rapproche -> couleur rouge
  if(distance_courante < distance_precedente)
   {
     couleur_affiche = 0; // rouge
     couleur_suppr = 2; // bleu
  }
  else // on s'éloigne couleur bleu
  {
    couleur_affiche = 2;
    couleur_suppr = 0;
  }
  couleur[couleur_affiche] += tmps;
  if(couleur[couleur_affiche] > 1.f) couleur[couleur_affiche] = 1.f;
  if(couleur[couleur_affiche] < 0.f) couleur[couleur_affiche] = 0.f;
  couleur[couleur_suppr] = 0.f;

  distance_precedente = distance_courante;

  if(distance_courante <= 5) return 1;
  else return 0;
}
