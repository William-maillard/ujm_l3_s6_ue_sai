#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique
#include "../inc/camera.h"
#include "../inc/collision.h"
#include "../inc/matrice.h"


float angle_phi;  // rotation verticale
float angle_theta; // rotation horizontale
float camera_orientation[3]; // direction de vue de la camera
float camera_up[3];
float camera_deplacement_lateral[3];

float position_point_de_vue[3];
float position_point_cible[3];


float vitesse_camera;
float vitesse_deplacement;

/* -- initialisation de la position de la caméra -- */
void initialiser_variables_camera() {
  camera_up[X] = 0.f;
  camera_up[Y] = 1.f;
  camera_up[Z] = 0.f;

  camera_deplacement_lateral[X] = -1.f;
  camera_deplacement_lateral[Y] = 0.f;
  camera_deplacement_lateral[Z] = 0.f;

  position_point_de_vue[X] = INTIT_JOUEUR_X;
  position_point_de_vue[Y] = INTIT_JOUEUR_Y;
  position_point_de_vue[Z] = INTIT_JOUEUR_Z;

  position_point_cible[X] = position_point_de_vue[X];
  position_point_cible[Y] = position_point_de_vue[Y];
  position_point_cible[Z] = position_point_de_vue[Z] - INTIT_JOUEUR_Z;

  vitesse_camera = 0.5f;
  vitesse_deplacement = 5.23f;//2.75f;



  /* -- calcule de l'angle de vue et de l'orientation de la caméra -- */
  vecteur_soustraction(
    position_point_cible, position_point_de_vue,
    camera_orientation
  );
  // indique une direction, il faut donc le normaliser
  vecteur_normaliser(camera_orientation);

  /* cf équation des coordonnées sphériques et exprimer theta et phi en
     fonction de x, y et z */
  angle_phi = asin(camera_orientation[Y]);//-35.6f;
  angle_theta = acos(camera_orientation[Z] / cos(angle_phi));//-135.0f;

  // si z négatif inverse l'angle, donc on le remet à l'endroit.
  if(camera_orientation[Z] < 0){
    angle_theta *= -1;
  }

  /* Conversion radian-degré :
     les fonction trigo renvoient un angle en radiant, or il nous le faut
     en degré pour pouvoir les utiliser avec le déplacement de la souris */
     angle_phi = angle_phi * 180.f / M_PI;
     angle_theta = angle_theta * 180.f / M_PI;


}


/* -- change la caméra de place -- */
void modifier_position_camera(int x, int y, int z) {

  /* -- changement de position -- */
  position_point_de_vue[X] = x;
  position_point_de_vue[Y] = y;
  position_point_de_vue[Z] = z;

  /* -- mise à jour du point ciblé -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
  );

}



/**
* Permet d'orienter la position de la caméra celon
* le déplacement de la souris.
* Phi : modifié par dy
* Theta : modifié par dx
*/
void orienter_camera(int dx, int dy) {
  float angle_phi_radian, angle_theta_radian;
  /* -- mise à jour des angles -- */
  angle_phi -= dy * vitesse_camera;
  angle_theta -= dx * vitesse_camera;

  /* -- limitation de l'angle phi -- */
  /* car si dépasse 90, on ne pourrat plus calculer la normal pour obtenir
     l'orientation de la caméra. */
  if(angle_phi > 89.0f) {
    angle_phi = 89.0f;
  }
  else if(angle_phi < -89.0f) {
    angle_phi = -89.0f;
  }

  /* -- conversion des degré en radiant -- */
  angle_phi_radian = angle_phi * M_PI / 180.f;
  angle_theta_radian = angle_theta * M_PI / 180.f;

  /* -- mise à jour de l'orientation de la caméra -- */
  // cf coordonnées sphériques
  camera_orientation[X] = cos(angle_phi_radian) * sin(angle_theta_radian);
  camera_orientation[Y] = sin(angle_phi_radian);
  camera_orientation[Z] = cos(angle_phi_radian) * cos(angle_theta_radian);


  /* -- calcul du nouveau vecteur latéral -- */
  vecteur_produit_vectoriel(
    camera_up, camera_orientation,
    camera_deplacement_lateral
  );
  vecteur_normaliser(camera_deplacement_lateral);

  /* -- calcul du nouveau point cible -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
   );

}



int avancer_camera() {
  float res[3]; // stocker le résultat du produit scalaire
  float tmps[3];// permet de reverser le déplacement

  /* -- sauvegarde de l'état courant -- */
    vecteur_copie(position_point_de_vue, tmps);

  /* -- calcul de la nouvelle position -- */
  /* on ajoute au vecteur position vitesse_deplacement
    fois le vecteur d'orientation de la caméra */
  vecteur_produit_scalaire(
    vitesse_deplacement, camera_orientation,
    res
  );
  vecteur_addition(
    position_point_de_vue, res,
    position_point_de_vue
  );

  /* -- test si collision avec un objet -- */
  if(collision_objets_camera())
  {
    // annule le déplacement
    vecteur_copie(tmps, position_point_de_vue);
    return 0;
  }


  /* -- mise à jour du point cible -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
  );

  return 1;
};


int reculer_camera() {
  float res[3]; // stocker le résultat du produit scalaire
  float tmps[3];// permet de reverser le déplacement

  /* -- sauvegarde de l'état courant -- */
    vecteur_copie(position_point_de_vue, tmps);

  /* -- calcul de la nouvelle position -- */
  /* on retire au vecteur position vitesse_deplacement
    fois le vecteur d'orientation de la caméra */
  vecteur_produit_scalaire(
    vitesse_deplacement, camera_orientation,
    res
  );
  vecteur_soustraction(
    position_point_de_vue, res,
    position_point_de_vue
  );

  /* -- test si collision avec un objet -- */
  if(collision_objets_camera())
   {
    // annule le déplacement
    vecteur_copie(tmps, position_point_de_vue);
    return 0;
  }

  /* -- mise à jour du point cible -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
  );

  return 1;
}


int deplacer_camera_gauche() {
  float res[3]; // stocker le résultat du produit scalaire
  float tmps[3];// permet de reverser le déplacement

  /* -- sauvegarde de l'état courant -- */
  vecteur_copie(position_point_de_vue, tmps);

  /* -- calcul de la nouvelle position -- */
  /* ajout du vecteur lateral à la position courante */
  vecteur_produit_scalaire(
    vitesse_deplacement, camera_deplacement_lateral,
    res
  );
  vecteur_addition(
    position_point_de_vue, res,
    position_point_de_vue
  );

  /* -- test si collision avec un objet -- */
  if(collision_objets_camera())
  {
    // annule le déplacement
    vecteur_copie(tmps, position_point_de_vue);
    return 0;
  }

  /* -- mise à jour du point cible -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
  );

  return 1;
}

int deplacer_camera_droite() {
  float res[3]; // stocker le résultat du produit scalaire
  float tmps[3];// permet de reverser le déplacement

  /* -- sauvegarde de l'état courant -- */
  vecteur_copie(position_point_de_vue, tmps);

  /* -- calcul de la nouvelle position -- */
  /* soustrait le vecteur lateral à la position courante */
  vecteur_produit_scalaire(
    vitesse_deplacement, camera_deplacement_lateral,
    res
  );
  vecteur_soustraction(
    position_point_de_vue, res,
    position_point_de_vue
  );

  /* -- test si collision avec un objet -- */
  if(collision_objets_camera())
  {
    // annule le déplacement
    vecteur_copie(tmps, position_point_de_vue);
    return 0;
  }

  /* -- mise à jour du point cible -- */
  vecteur_addition(
    position_point_de_vue, camera_orientation,
    position_point_cible
  );

  return 1;
}
