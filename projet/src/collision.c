/***
* Fonctions permettant de gérer les collisions de la caméra avec les
* objets du 8 arbre.
****/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "GL/gl.h"
//#include "GL/glut.h"
#include "../inc/8-arbre.h"
#include "../inc/camera.h"
#include "../inc/couleur.h"
#include "../inc/matrice.h"
#include "../inc/objet.h"
#include "../inc/parametres.h"
#include "../inc/collision.h"


/****
* Retourne 1 si les cubes s'intersectent, 0 sinon.
****/
static int collision_cube(cube c1, cube c2) {
  return (
    c1.p2[X] >= c2.p1[X]  &&  c1.p1[X] <= c2.p2[X]  &&
    c1.p2[Y] >= c2.p1[Y]  &&  c1.p1[Y] <= c2.p2[Y]  &&
    c1.p2[Z] <= c2.p1[Z]  &&  c1.p1[Z] >= c2.p2[Z]
  ) ? 1 : 0;
}


/****
* retourne 1 si un objet intersecte la caméra, 0 sinon.
****/
int collision_objets_camera(){
  cube cube_camera = {
    {
      position_point_de_vue[X] - DISTANCE_COLLISION,
      position_point_de_vue[Y] - DISTANCE_COLLISION,
      position_point_de_vue[Z] - DISTANCE_COLLISION
    },
    {
      position_point_de_vue[X] + DISTANCE_COLLISION,
      position_point_de_vue[Y] + DISTANCE_COLLISION,
      position_point_de_vue[Z] + DISTANCE_COLLISION
    },
    DISTANCE_COLLISION
  };
  arbre noeud_courant = arbre_monde;
  int i;

  /* -- on cherche dans quel partie de l'arbre se trouve la caméra -- */
  while(!est_feuille(noeud_courant))
  {
    noeud_courant = noeud_courant->fils[
      trouver_index_subdivision(
        position_point_de_vue,
        noeud_courant->position_centre
      )
    ];
  }

  /* -- on cherche si elle intersecte un objet de la zone -- */
  for(i=0; i<noeud_courant->nb_objets; i++)
  {
    // NB ici tester le type (objet.type == MAISON) avant de faire objet.maison
    if(collision_cube(cube_camera, noeud_courant->objets[i].maison.cube_englobant))
    {
      return 1;
    }
  }

  /* -- pas de collisions détectées -- */
  return 0;
}
