/********************************************************************
 *                                                                  *
 * Structure de matrice pour représenter un point dans le plan 2D   *
 * (avec des coordonnees homogenes)                                 *
 * Structure de matrice.                                            *
 *                                                                  *
 ********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../inc/matrice.h"


/*** copie la matrice a dans b ***/
void mat_copie(matrice a, matrice b) {
    int i, j;

    for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
            b[i][j] = a[i][j];
        }
    }
}

/**** calcule dans c la matrice a*b ****/
void mat_multiplication(matrice a, matrice b, matrice c) {
    int i, j;

    for(i=0; i<N; i++) {
        memset(c[i], 0, N); // init la ligne a 0
        for(j=0; j<N; j++) {
            c[i][j] += a[i][j]*b[j][i];
        }
    }
}

void affiche_point(point p) {
    printf("(%d, %d, %d)\n", p.x, p.y, p.w);
}

/**** calcule dans c le point p multiplie par a ****/
void point_multiplication_mat(matrice a, point p, point *c) {

    printf("Point depart :"); affiche_point(p);

    c->x  = a[0][0]*p.x + a[0][1]*p.y + a[0][2]*p.w;
    c->y  = a[1][0]*p.x + a[1][1]*p.y + a[1][2]*p.w;
    c->w  = a[2][0]*p.x + a[2][1]*p.y + a[2][2]*p.w;

    printf("Point final :"); affiche_point(*c);
}


/**** calcule dans w le produit vectoriel de deux vecteurs ****/
// vect de dimension 3, attension w doit pointer sur un sespace mémoire alloué
void vecteur_produit_vectoriel(float *u, float *v, float *w) {
  float w2[3]; // au cas ou w soit aussi egal à u ou v

  /* -- calcul du produit vectoriel -- */
  w2[X] = u[Y]*v[Z] - u[Z]*v[Y];
  w2[Y] = u[Z]*v[X] - u[X]*v[Z];
  w2[Z] = u[X]*v[Y] - u[Y]*v[X];

  /* -- copie du résultat dans w -- */
  w[X] = w2[X];
  w[Y] = w2[Y];
  w[Z] = w2[Z];
}

/**** normalise le vecteur passé en paramètre ****/
void vecteur_normaliser(float *v) {
  float norme = sqrt(pow(v[X], 2) + pow(v[Y], 2) + pow(v[Z], 2));

  v[X] /= norme;
  v[Y] /= norme;
  v[Z] /= norme;
}


/**** calcule l'addition de u et v dans w ****/
// w doit être alloué
// w peut être égal à u ou void
void vecteur_addition(float *u, float *v, float *w) {
  w[X] = u[X] + v[X];
  w[Y] = u[Y] + v[Y];
  w[Z] = u[Z] + v[Z];
}

/****  calcule la soustraction de u par v dans w ****/
void vecteur_soustraction(float *u, float *v, float *w) {
  w[X] = u[X] - v[X];
  w[Y] = u[Y] - v[Y];
  w[Z] = u[Z] - v[Z];
}

/***  calcul dans w le produit de v avec le scalaire k ****/
void vecteur_produit_scalaire(float k, float *v, float *w) {
  w[X] = v[X] * k;
  w[Y] = v[Y] * k;
  w[Z] = v[Z] * k;
}

/****  copie le vecteur u dans le vecteur w ****/
void vecteur_copie(float u[3], float w[3]) {
  w[X] = u[X];
  w[Y] = u[Y];
  w[Z] = u[Z];
}
