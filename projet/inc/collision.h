#ifndef _COLLISION_H_
#define _COLLISION_H_

/****
* retourne 1 si un objet intersecte la caméra, 0 sinon.
****/
int collision_objets_camera();

#endif
