/**
* Ce module permet de créer les différents objets
* composant l'environnement 3D du projet :
*   - maison
*   - ...
*/
#ifndef _OBJET_H_
#define _OBJET_H_


typedef enum e_type_objet {
  MAISON
} type_objet;

/*
  Un cube est définie par 2 points et une dimension de côté :
    - p1 est celui qui a les coordonnées minimales
    - p2 est celui qui a les coordonnées maximales
*/
typedef struct struct_cube {
  float p1[3];
  float p2[3];
  float cote;
} cube;


typedef struct struct_maison {
  type_objet type;
  cube cube_englobant;
  float hauteur_mur;
  float pente_toit;
} maison;


/*
  Pour pouvoir stocker différents objects du jeu dans le 8-arbre de l'esapce.
*/
typedef union u_objet {
  type_objet type;
  maison maison;
} objet;




/* ***
* dessine un cube plein (plein = 1) ou vide (plein = 0).
*** */
void dessiner_cube(cube cube, int plein, float couleur[3]);

/* *** dessine la maison donnéee en paramètre *** */
void dessiner_maison(maison maison);

/* -- permet de distinguer la maison cible des autres maisons -- */
void dessiner_distinction_maison_cible();

/****
* Créer une maison à la position (x, y, z) avec des paramètres aléatoires.
****/
maison creer_maison_alea(float x, float y, float z);

#endif
