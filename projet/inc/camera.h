#ifndef _CHAMERA_H_
#define _CHAMERA_H_

#include "parametres.h"


extern float angle_phi;  // rotation verticale
extern float angle_theta; // rotation horizontale
extern float camera_orientation[3]; // direction de vue de la camera
extern float camera_up[3];
extern float camera_deplacement_lateral[3];

extern float position_point_de_vue[3];
extern float position_point_cible[3];


extern float vitesse_camera;
extern float vitesse_deplacement;

/* -- initialisation de la position de la caméra -- */
void initialiser_variables_camera();

/* -- change la caméra de place -- */
void modifier_position_camera(int x, int y, int z);

/**
* Permet d'orienter la position de la caméra celon
* le déplacement de la souris.
* Phi : modifié par dy
* Theta : modifié par dx
*/
void orienter_camera(int dx, int dy);

int avancer_camera();
int reculer_camera();
int deplacer_camera_gauche();
int deplacer_camera_droite();


#endif
