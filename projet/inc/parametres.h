/**********************************************
*                                             *
* Contient des macros et variables globales   *
* pour paramètrer le jeu.                     *
*                                             *
***********************************************/
#ifndef _PARAMETRE_H_
#define _PARAMETRE_H_

// position de départ du joueur

#define DISTANCE_JOUEUR_ORIGINE 100
#define INTIT_JOUEUR_X 0.f
#define INTIT_JOUEUR_Y 100.f // taille du joeuur
#define INTIT_JOUEUR_Z 100.f


/* -- dimension de la fenetre -- */
extern float bordure_fenetre[4];
typedef enum {
  F_HAUT=0, F_BAS, F_GAUCHE, F_DROIT
}cote_fenetre;

// défini la zone visible à l'écran
#define ANGLE_DE_VU 60.0f
#define DROITE 50
#define GAUCHE -DROITE
#define HAUT 50
#define BAS -HAUT
#define AVANT 0.1f
#define ARRIERE 70000.0f


// limites du monde
// monde contenu dans un cube de taille 2^N, N = 14
#define WORLD_COTE_CUBE 2048 /*9192//16384*/

#define WORLD_WIDTH WORLD_COTE_CUBE
#define WORLD_HEIGHT WORLD_COTE_CUBE
#define WORLD_DEPTH WORLD_COTE_CUBE

#define WORLD_BOUND_X WORLD_WIDTH/2.f
#define WORLD_BOUND_Y WORLD_HEIGHT/2.f
#define WORLD_BOUND_Z WORLD_DEPTH/2.f



// pour la distance des collisions entre camera et nb_objets
#define DISTANCE_COLLISION 2.f

// combien de temps avant d'actualiser l'indice de distance (en s)
#define TEMPS_RAFRAICHISSEMENT_INDICE 2.0f


/* -- Affichage du 8-arbre en fil de fer : 1 activé, 0 désactivé. -- */
extern int affichage_abre;

/* -- Affichage des boites englobantes : 1 activé, 0 désactivé. -- */
extern int dessiner_cube_englobant;

/* -- indice pour la maison à trouver -- */
extern int afficher_indice;
extern float distance_precedente;
extern float couleur[3];

#endif
