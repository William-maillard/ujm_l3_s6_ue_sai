#ifndef _CHARGEMENT_IMAGE_BMP_H_
#define  _CHARGEMENT_IMAGE_BMP_H_


typedef struct struct_image {
  int longueur, largeur;
  unsigned char *donnees;
}image;


/****
*  charche et renvoie une image à partir du fichier
*  dont le chemin est passé en paramètre.
* Renvoie un pointeur sur l'image ou NULL.
****/
image *bmp_chargement(char *fichier);


#endif
