/********************************************************************
 *                                                                  *
 * Structure de matrice pour représenter un point dans le plan 2D   *
 * (avec des coordonnees homogenes)                                 *
 * Structure de matrice.                                            *
 *                                                                  *
 ********************************************************************/
#ifndef _MATRICE_H_
#define _MATRICE_H_

// pour les tableaux représenatant un vecteur
enum{X, Y, Z, W};

#define N 3


typedef struct {
    int x, y, w;
}point;


typedef double matrice[N][N];

/*** copie la matrice a dans b ***/
void mat_copie(matrice a, matrice b);

/**** calcule dans c la matrice a*b ****/
void mat_multiplication(matrice a, matrice b, matrice c);


/**** calcule dans c le point p multiplie par a ****/
void point_multiplication_mat(matrice a, point p, point *c);


/**** calcule dans w le produit vectoriel de deux vecteurs ****/
// vect de dimension 3, attension w doit pointer sur un sespace mémoire alloué
// w peut être égal à u ou v
void vecteur_produit_vectoriel(float *u, float *v, float *w);



/**** normalise le vecteur passé en paramètre ****/
void vecteur_normaliser(float *v) ;

/**** calcule l'addition de u et v dans w ****/
// w doit être alloué
// w peut être égal à u ou void
void vecteur_addition(float *u, float *v, float *w);

/****  calcule la soustraction de u par v dans w ****/
void vecteur_soustraction(float *u, float *v, float *w);


/***  calcul dans w le produit de v avec le scalaire k ****/
void vecteur_produit_scalaire(float k, float *v, float *w);

/****  copie le vecteur u dans le vecteur w ****/
void vecteur_copie(float u[3], float w[3]);

#endif
