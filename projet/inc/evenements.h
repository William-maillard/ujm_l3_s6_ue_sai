/******************************************************
*                                                     *
* Déclaration des fonctions pour gérer les évènements *
* d'openGL.                                           *
*                                                     *
*******************************************************/
#ifndef _EVENEMENTS_H_
#define _EVENEMENTS_H_


/* -- AFFICHAGE -- */
void afficher_scene();
void redimensionner_fenetre();
void actualiser_animation();
void fonction_periodique(int valeur);


/* -- CLAVIER -- */
void traiter_entree_clavier(unsigned char key, int x, int y);
void traiter_entree_clavier_special(int key, int x, int y);


/* -- SOURIS -- */
void taiter_click_souris();
void traiter_mouvement_souris_avec_bouton_click();
void traiter_mouvement_souris();


#endif
