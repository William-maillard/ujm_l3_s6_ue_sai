/**
* Défini des macros pour être utilisés en
* paramètre de la fonction glColor3.
* Permet aussi d'initialiser un tableau pour le
* passer en paramètre de glColor3fv((float[]){COULEUR_})
**/
#ifndef _COULEUR_H_
#define _COULEUR_H_


#define COULEUR_PAR_DEFAUT 0.f, 0.f, 1.f

#define COULEUR_CIEL 0.8f, 1.f, 1.f
#define COULEUR_HERBE 0.2f, 0.4f, 0.f
#define COULEUR_TERRE 0.2f, 0.05f, 0.f

#define COULEUR_TOIT 0.8f, 0.2f, 0.f
#define COULEUR_MUR 1.f, 0.75f, 0.f

#define COULEUR_CIBLE 1.f, 0.f, 1.f


#endif
