/***
* Fonctions permettant d'avoir un petit jeu de
* recherche de maison avec un indicateur pour savoir si on
* se rapproche ou non de l'objectif.
****/
#ifndef _JEU_H_
#define _JEU_H_

/****
* Initialise la distance entre la camera et la maison cible
****/
void init_distance_precedente();


/****
* Renvoie 1 si la maison a ete trouve, 0 sinon.
****/
int maison_trouve();

#endif
