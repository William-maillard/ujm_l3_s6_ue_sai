/***
* Structure d'octree pour stocker les objets de l'environnement.
* Un noeud représente une subdivision de l'espace (=1 cube),
* représenté par son centre et une taille.
****/
#ifndef _8_ARBRE_H_
#define _8_ARBRE_H_

#include "../inc/objet.h"
#include "../inc/parametres.h"

#define NOMBRE_MAX_OBJETS_PAR_CUBE 5

typedef struct struct_noeud {
  float position_centre[3];
  float taille_cote;
  struct struct_noeud **fils;
  int nb_objets;
  objet *objets;
}noeud;

typedef noeud *arbre;

extern arbre arbre_monde;
extern maison maison_cible;

/*
  Enumère les différentes positions possibles pour une subdivision d'un cube
  en se servant de 3 bits représentant chacun une dimension.
  BAS : 0__
  HAUT : 1__
  GAUCHE : _0_
  DROITE : _1_
  AVANT : __0
  ARRIERE : __1
*/
enum index_subivision_cube {
  A_BAS_GAUCHE_AVANT = 0, // 000
  A_BAS_DROITE_AVANT = 2, // 010
  A_BAS_DROITE_ARRIERE = 3, // 011
  A_BAS_GAUCHE_ARRIERE = 1, // 001
  A_HAUT_GAUCHE_AVANT = 4, // 100
  A_HAUT_DROITE_AVANT = 6, // 110
  A_HAUT_DROITE_ARRIERE = 7, // 111
  A_HAUT_GAUCHE_ARRIERE = 5, // 101
};

/*
  Permet de donner une valeur a une localisation par rapport à sa
  représentation 3 bits.
*/
#define A_BAS 0     // 0__
#define A_HAUT 4    // 1__
#define A_GAUCHE 0  // _0_
#define A_DROITE 2  // _1_
#define A_AVANT 0   // __0
#define A_ARRIERE 1 // __1


/* permet de trouver l'index de la subdivision du cube auquel appartient
   le vecteur.
   Renvoie cet index */
int trouver_index_position(float position[3], float centre_subdivision[3]);


/****
* Creer un noeud avec la position de la subdivision du cube et sa taille
* passé en paramètre.
****/
arbre creer_noeud(float position_centre[3], int taille_cote);


/****
* renvoie 1 si le noeud est une feuille,
* 0 sinon.
****/
int est_feuille(arbre noeud);


/****
* Initialise l'arbre du programme avec 1 racine et 8 feuilles.
****/
void initialiser_arbre();


/****
* permet de trouver l'index de la subdivision du cube auquel appartient
* le vecteur.
* Renvoie cet index.
****/
int trouver_index_subdivision(float position[3], float centre_subdivision[3]);


/****
* Divise en 8 subdivisions de l'espace le noeud passé en paramètre.
* Renvoie 1 si le noeud a été subdiviser,
* 0 sin on ne peut plus le subdiviser.
****/
int subdiviser_arbre(arbre noeud);


/****
* Insère l'objet dans la zone du 8-abrepar rapport à sa position.
****/
int inserer_objet(arbre arbre, objet objet);



/*****
* Dessine les objets contenu dans le 8-arbre.
* Si la variable affichage_arbre = 1 alors
* dessine le 8-arbre en fil de fer.
****/
void dessiner_objets(arbre arbre) ;

#endif
