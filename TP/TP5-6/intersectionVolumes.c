#include "k-arbre.h"
#include "volume2arbre.h"


karbre intersection(karbre V1, karbre V2) {
    if(V1 == NULL  ||  V2 == NULL)
        return kArbreVide();

    /* Si 1 des 2 est vide alors l'intersection est vide */
    if(kRacine(V1) == K_ARBRE_VOLUME_VIDE  ||  kRacine(V2) == K_ARBRE_VOLUME_VIDE) {
        return kConsArbre(K_ARBRE_VOLUME_VIDE, 0);
    }

    /* si l'un des deux est plein alors l'intersection est égale au second */
    if(kRacine(V1) == K_ARBRE_VOLUME_PLEIN) {
        return kCopie(V2);
    }
    if(kRacine(V2) == K_ARBRE_VOLUME_PLEIN) {
        return kCopie(V1);
    }

    /* sinon on regarde recursivement l'intersection des sous-arbres */
    return kConsArbre(K_ARBRE_VOLUME_COMPLEXE,
                      intersection(kFils(0, V1), kFils(0, V2)),
                      intersection(kFils(1, V1), kFils(1, V2)),
                      intersection(kFils(2, V1), kFils(2, V2)),
                      intersection(kFils(3, V1), kFils(3, V2)),
                      intersection(kFils(4, V1), kFils(4, V2)),
                      intersection(kFils(5, V1), kFils(5, V2)),
                      intersection(kFils(6, V1), kFils(6, V2)),
                      intersection(kFils(7, V1), kFils(7, V2))
        );
}