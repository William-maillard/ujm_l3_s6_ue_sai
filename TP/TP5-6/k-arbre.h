/***********************
 *     TAD k-arbre     *
 ***********************/
#ifndef _K_ARBRE_H_
#define _K_ARBRE_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define K 8 // arite des arbres du TAD

typedef int element;

struct noeud {
    element valeur;
    int nbFils;
    struct noeud* fils[K];
};

typedef struct noeud * karbre;

/* *** renvoie l'arbre vide *** */
karbre kArbreVide();

/* *** 
 * cree un noeud contenant l'element e et les N fils passes en parametres
 * ( 0 <= N <= K)  Le deuxieme argument est le nb de fils donnes
 *** */
karbre kConsArbre(element e, int n, ...);

/* *** renvoie le i° fils de l'arbre A *** */
karbre kFils(int ieme, karbre A);

/* *** renvoie l'element stocker a la racine de A *** */
element kRacine(karbre A);

/* *** test si l'arbre est vide : 1 si oui, 0 sinon *** */
int kEstVide(karbre A);

/* *** affiche l'arbre A avec des elements entiers *** */
void kAfficher(karbre A);

/* cree la copie de l'arbre A */
karbre kCopie(karbre A);


#endif