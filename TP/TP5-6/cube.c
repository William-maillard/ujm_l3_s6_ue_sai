#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h" // OpenGL
/**** 
      Affiche un cube a partir de 2 point :
        > le 1 a les coord les plus petites
        > le 2 les plus grandes
***/
void affiche_cube(int x1, int y1, int z1, int x2, int y2, int z2) {
    glBegin(GL_QUADS);
    
    // face arriere :
    glColor3f(0.9, 0.9, 0.9);
    glVertex3i(x2, y2, z2);
    glVertex3i(x1, y2, z2);
    glVertex3i(x1, y1, z2);
    glVertex3i(x2, y1, z2);

    
    // face gauche :
    glColor3f(0.8, 0.8, 0.8);
    glVertex3i(x1, y2, z2);
    glVertex3i(x1, y2, z1);
    glVertex3i(x1, y1, z1);
    glVertex3i(x1, y1, z2);

    // face dessous :
    glColor3f(0.4, 0.4, 0.4);
    glVertex3i(x1, y1, z1);
    glVertex3i(x2, y1, z1);
    glVertex3i(x2, y1, z2);
    glVertex3i(x1, y1, z2);
    
    // face avant :
    glColor3f(0.7, 0.7, 0.7);
    glVertex3i(x1, y1, z1);
    glVertex3i(x1, y2, z1);
    glVertex3i(x2, y2, z1);
    glVertex3i(x2, y1, z1);

    // face droite :
    glColor3f(0.6, 0.6, 0.6);
    glVertex3i(x2, y2, z2);
    glVertex3i(x2, y1, z2);
    glVertex3i(x2, y1, z1);
    glVertex3i(x2, y2, z1);
    
    // face dessus :
    glColor3f(0.5, 0.5, 0.5);
    glVertex3i(x2, y2, z2);
    glVertex3i(x1, y2, z2);
    glVertex3i(x1, y2, z1);
    glVertex3i(x2, y2, z1);
    
    glEnd();
}