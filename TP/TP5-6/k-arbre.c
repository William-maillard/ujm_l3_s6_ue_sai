/***********************************
 *                                 *
 * Declaration des fonctions pour  *
 * manipuler un k-arbre            *
 *                                 *
 ***********************************/
#include "k-arbre.h"

/* *** renvoie l'arbre vide *** */
karbre kArbreVide() {  return NULL;  }


/* *** cree un noeud contenant l'element e et les N fils passes en parametres
   ( 0 <= N <= K). Le deuxieme argument est le nb de fils donnes *** */
karbre kConsArbre(element e, int n, ...) {
    va_list params; // liste des arguments
    karbre A;
    int i;
    
    
    /* -- creation du noeud A -- */
    A = (karbre) malloc(sizeof(struct noeud));
    if(A == NULL) {
        fprintf(stderr, "(kConsArbre) pbm malloc.\n");
        exit(-1);
    }
    A->valeur = e;
    A->nbFils = 0;

    /* -- affectation des fils au noeud A -- */
    va_start(params, n); /* initialise le pointeur grace 
                            au dernier parametre fixe    */
    for(i=0; i<n; i++) {
        A->fils[i] = va_arg(params, karbre);
        A->nbFils++;
    }
    va_end(params); // fermeture de la liste

    /* initialise a l'arbre vide les autres fils de A */
    for(i=A->nbFils; i<K; i++) {
        A->fils[i] = kArbreVide();
    }

    return A;
}


/* *** renvoie le i° fils de l'arbre A, null si pas de ieme fils *** */
karbre kFils(int ieme, karbre A) {  return A->fils[ieme];  }


/* *** renvoie l'element stocker a la racine de A *** */
element kRacine(karbre A) {  return A->valeur;  }


/* *** test si l'arbre est vide : 1 si oui, 0 sinon *** */
int kEstVide(karbre A) {  return (A == kArbreVide())? 1 : 0;  }


/* *** affiche l'arbre A avec des elements entiers *** */
static void kAfficher_bis(karbre A, int prof) {
    int i=0;
    
    printf("%*c|%d\n", prof, ' ', A->valeur);

    // NB : pour une feuille, on ne rentre pas dans la boucle.
    while(i < K  &&  A->fils[i] != kArbreVide()) {
        kAfficher_bis(A->fils[i], prof+1);
        i++;
    }
}

void kAfficher(karbre A) {
    kAfficher_bis(A, 0);
}


/* cree la copie de l'arbre A */
karbre kCopie(karbre A) {

    if(A == kArbreVide())
        return kArbreVide();

    return kConsArbre(kRacine(A), 8,
                 kCopie(kFils(0, A)), kCopie(kFils(1, A)), kCopie(kFils(2, A)),
                 kCopie(kFils(3, A)), kCopie(kFils(4, A)), kCopie(kFils(5, A)),
                 kCopie(kFils(6, A)), kCopie(kFils(7, A))
        );
}