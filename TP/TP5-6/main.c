#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique
#include "k-arbre.h"
#include "volume2arbre.h"
#include "cube.h"

#define DROIT 50
#define GAUCHE -DROIT
#define HAUT 50
#define BAS -HAUT
#define AVANT 20
#define ARRIERE 100

float angle = 0;

/****
     Affiche un k_arbre représentant un volume
/****
void affiche_arbre_volume(karbre a) {
    if(a == null)
        return;

    if(a->valeur == K_ARBRE_VOLUME_PLEIN)

    for(int i=0; i<K; i++) {

    }
}
*/

/*** fonction d'affichage de la fenetre graphique ***/
void Affichage() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* -- Init matrice visualisation a la matrice identite -- */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* -- definition du point de vue de l'utilisateur -- */
    glFrustum(GAUCHE, DROIT, BAS, HAUT, AVANT, ARRIERE);
    gluLookAt(100, 50, 0, 5, 0, 0, 0, HAUT, 0);
    glRotatef(angle,0,0,1);

    /* -- affichage de la figure -- */
    //glBegin(GL_QUAD_STRIP);
    glBegin(GL_QUADS);
    /* glVertex3i(0, 0, 5);
       glVertex3i(0, 0, -5);
       glVertex3i(0, 10, -5);
       glVertex3i(0, 10, 5);
    */
    /*
    //face avant :
    glColor3f(0.9, 0.9, 0.9);
    glVertex3i(-5, 0, 5);
    glVertex3i(5, 0, 5);
    glVertex3i(5, 10, 5);
    glVertex3i(-5, 10, 5);

    ///*
    // face gauche :
    glColor3f(0.8, 0.8, 0.8);
    glVertex3i(-5, 10, 5);
    glVertex3i(-5, 10, -5);
    glVertex3i(-5, 0, -5);
    glVertex3i(-5, 0, 5);

    // face dessous :
    glColor3f(0.4, 0.4, 0.4);
    glVertex3i(-5, 0, 5);
    glVertex3i(5, 0, 5);
    glVertex3i(5, 0, -5);
    glVertex3i(-5, 0, -5);

    // face arriere :
    glColor3f(0.7, 0.7, 0.7);
    glVertex3i(-5, 0, -5);
    glVertex3i(-5, 10, -5);
    glVertex3i(5, 10, -5);
    glVertex3i(5, 10, -5);

    // face droite :
    glColor3f(0.6, 0.6, 0.6);
    glVertex3i(5, 10, -5);
    glVertex3i(5, 0, -5);
    glVertex3i(5, 0, 5);
    glVertex3i(5, 10, 5);

    // face dessus :
    glColor3f(0.5, 0.5, 0.5);
    glVertex3i(5, 10, 5);
    glVertex3i(5, 10, -5);
    glVertex3i(-5, 10, -5);
    glVertex3i(-5, 10, 5);

    */
    //glEnd();

    //affiche_cube(-5, 0, 5, 5, 10, -5);
    boule2arbre(0, 0, 0, 20);

    glutSwapBuffers();
    //glFlush(); // envoie au buffer d'affichage
}


/*** fonction de traitement des evenements clavier ***/
void Clavier(unsigned char key, int x, int y) {
    if(key == 'a')
        angle *= -1;
}

/*** Fonction pour faire tourner la vue de l'utilisateur ***/
void Animer() {
    angle += .010;
    if (angle > 360) angle = 0;
    glutPostRedisplay();
}

void usage(char *s) {
    exit(0);
}


int main(int argc, char **argv) {

    /* -- Initialisation de GLUT -- */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);
    /* GLU_DEPTH est un buffer pour gérer les faces cachés */

    /* -- Position et taille de la fenetre -- */
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    // glutFullScreen();
        glEnable(GL_DEPTH_TEST);

    /* -- Creation de la fenetre -- */
    glutCreateWindow("Une maison");
    /* NB : retour un entier pour ientifier la fenetre */

    /* -- fonctions speciales de la fenetre -- */
    glutDisplayFunc(Affichage);
    glutKeyboardFunc(Clavier);
    glutIdleFunc(Animer);

    glutMainLoop();
    return 0;
}
