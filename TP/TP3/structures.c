/********************************************************************
 *                                                                  *
 * Structure de matrice pour représenter un point dans le plan 2D   *
 * (avec des coordonnees homogenes)                                 *
 * Structure de matrice.                                            *
 *                                                                  *
 ********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../inc/matrice.c"


/*** copie la matrice a dans b ***/
void mat_copie(matrice a, matrice b) {
    int i, j;

    for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
            b[i][j] = a[i][j];
        }
    }
}

/**** calcule dans c la matrice a*b ****/
void mat_multiplication(matrice a, matrice b, matrice c) {
    int i, j;

    for(i=0; i<N; i++) {
        memset(c[i], 0, N); // init la ligne a 0
        for(j=0; j<N; j++) {
            c[i][j] += a[i][j]*b[j][i];
        }
    }
}

void affiche_point(point p) {
    printf("(%d, %d, %d)\n", p.x, p.y, p.w);
}

/**** calcule dans c le point p multiplie par a ****/
void point_multiplication_mat(matrice a, point p, point *c) {

    printf("Point depart :"); affiche_point(p);

    c->x  = a[0][0]*p.x + a[0][1]*p.y + a[0][2]*p.w;
    c->y  = a[1][0]*p.x + a[1][1]*p.y + a[1][2]*p.w;
    c->w  = a[2][0]*p.x + a[2][1]*p.y + a[2][2]*p.w;

    printf("Point final :"); affiche_point(*c);
}
