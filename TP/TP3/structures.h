/********************************************************************
 *                                                                  *
 * Structure de matrice pour représenter un point dans le plan 2D   *
 * (avec des coordonnees homogenes)                                 *
 * Structure de matrice.                                            *
 *                                                                  *
 ********************************************************************/
#ifndef _MATRICE_H_
#define _MATRICE_H_

#define N 3


typedef struct {
    int x, y, w;
}point;


typedef double matrice[N][N];

/*** copie la matrice a dans b ***/
void mat_copie(matrice a, matrice b);

/**** calcule dans c la matrice a*b ****/
void mat_multiplication(matrice a, matrice b, matrice c);


/**** calcule dans c le point p multiplie par a ****/
void point_multiplication_mat(matrice a, point p, point *c);

#endif
