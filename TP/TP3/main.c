#include <math.h> // M_PI
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // pour memset
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique*
#include "structures.h"

#define NB_PIX_X 68//544//272//136//68
#define NB_PIX_Y 34//272//136//68//34
#define TAILLE_PIXEL 0.5


/* -- Definition des points de l'exercice -- */
point A = {-1, -1, 1}, B = {3, 0, 1}, C = {1, 2, 1}, D = {-1, 3, 1};
point Oprim = {2, 1, 1};
point t = {-3, -1, 1};

/* matrice de changement d'echelle par rapport a l'origine 
   d'un facteur 2 selon Ox et 3 selon Oy */
matrice S =
{ {2, 0, 0},
  {0, 3, 0},
  {0, 0, 1}
};

/* matrice de rotation d'un angle de pi/3 par rapport a 0prim */
matrice R =
{ {cos(M_PI/3.), -sin(M_PI/3.), -2*cos(M_PI/3.) + sin(M_PI/3.) + 2},
  {sin(M_PI/3.), cos(M_PI/3.), -2*sin(M_PI/3.) - cos(M_PI/3.) + 1},
  {0, 0, 1}
};

/* matrice de translation associe au vecteur t */
matrice T =
{ {1, 0, -3},
  {0, 1, -1},
  {0, 0, 1}
};

/* matrice transformation equivalente a R suivi de T suivi de S */
matrice T1, tmps;
//mat_multiplication(S, T, T1);
//mat_multiplication(T1, R, tmps);
//mat_copie(tmps, T1);

// pour retenir la derniere transformation effectue
typedef enum{T_S, T_R, T_T} transformation;
transformation tr = T_S;

/* ----------------------------------------- */


/*** fonction d'affichage de la fenetre graphique ***/
void Affichage() {
    printf("Affichage\n");
    glClear(GL_COLOR_BUFFER_BIT); // remplis couleur par defaut
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-NB_PIX_X/2, NB_PIX_X/2, -NB_PIX_Y, NB_PIX_Y, 0, 1);
    /* projection orthogonal des points dans 
       xmin, xmax, ymin, ymax, zmin, zmax */
    
    /* -- Init matrice visualisation a la matrice identite -- */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* -- affichage de la figure -- */
    glBegin(GL_POLYGON);
    glVertex2i(A.x, A.y);
    glVertex2i(B.x, B.y);
    glVertex2i(C.x, C.y);
    glVertex2i(D.x, D.y);
    glEnd();
    
    glFlush(); // envoie au buffer d'affichage
}


/*** fonction de traitement des evenements clavier ***/
void Clavier(unsigned char key, int x, int y) {
    matrice M;
    
    switch(tr) {
    case T_S:
        printf("transformation S\n");
        point_multiplication_mat(S, A, &A);
        point_multiplication_mat(S, B, &B);
        point_multiplication_mat(S, C, &C);
        point_multiplication_mat(S, D, &D);
        tr++;
        break;
    case T_R:
        printf("transformation R\n");
        point_multiplication_mat(R, A, &A);
        point_multiplication_mat(R, B, &B);
        point_multiplication_mat(R, C, &C);
        point_multiplication_mat(R, D, &D);
        tr++;
        break;
    case T_T:
        printf("transformation T\n");
        point_multiplication_mat(T, A, &A);
        point_multiplication_mat(T, B, &B);
        point_multiplication_mat(T, C, &C);
        point_multiplication_mat(T, D, &D);
        tr = T_S;
        break;
    }

    Affichage();
}


void usage(char *s) {
    printf("usage %s : num_exo <autre param>.\n", s);
    printf("si num exo = 1 : \n");


    exit(0);
}


int main(int argc, char **argv) {
    
    /* -- Initialisation de GLUT -- */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);

    /* -- Position et taille de la fenetre -- */
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    // glutFullScreen();

    /* -- Creation de la fenetre -- */
    glutCreateWindow("Une fenetre OpenGL");
    /* NB : retour un entier pour ientifier la fenetre */

    /* -- fonctions speciales de la fenetre -- */
    glutDisplayFunc(Affichage);
    glutKeyboardFunc(Clavier);

    glutMainLoop();    
    return 0;
}