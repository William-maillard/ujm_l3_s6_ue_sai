#include "k-arbre.h"
#include "volume2arbre.h"

/* renvoie un arbre correspondant à un arbre plein ou vide */
karbre kArbre_volumevide() {
    return kConsArbre(K_ARBRE_VOLUME_VIDE, 0);
}
karbre kArbre_volumeplein() {
    return kConsArbre(K_ARBRE_VOLUME_PLEIN, 0);
}

/* renvoie la valeur absolu de x */
int abs(int x) {  return (x < 0)? -x : x;  }

/* *** 
 * renvoie le 8-arbre representant le volume de la sphere 
 * de centre (x, y, z) et de rayon r.
 *** */
static karbre boule2arbre_bis(int x, int y, int z, int r, int x1, int y1, int z1, int x2, int y2, int z2, int prof)
{
    int rb, xb, yb, zb; // pour l'approximation du cube en boule
    int d; // pour la distance entre les 2 centre.

    
    /* -- si on a atteid la prof max on arrete les subdivisions -- */
    if(prof == PROFONDEUR_MAX) {
        return kArbre_volumevide();
    }
    
    /* -- On teste l'inclusion du cube dans la boule -- */
    if(abs(x1-x) <= r  &&  abs(y1-y) <= r  &&  abs(z1-z) <= r  &&
       abs(x2-x) <= r  &&  abs(y2-y) <= r  &&  abs(z2-z) <= r )
    {
        return kArbre_volumeplein();
    }

    /* -- On teste l'intersection du cube avec la boule -- */
    rb = abs(x2-x1)+r;
    xb = (x1+x2)/2; yb = (y1+y2)/2; zb = (z1+z2)/2;
    
    if(!( (d = abs(x-xb)) <= rb &&
          (d = abs(y-yb)) <= rb &&
          (d = abs(z-zb)) <= rb    ))
    {
        return kArbre_volumevide();
    }
    
    
    /* -- On subdive l'espace en 8 cubes -- */
    /* Face avant :    Face arriere :
       |  1  |  2  |   |  5  |  6  |
       |  3  |  4  |   |  7  |  8  |
    */
    // milieu des arretes du cube sur les diff coord
    int xM = (x1+x2)/2;
    int yM = (y1+y2)/2;
    int zM = (z1+z2)/2;

    return kConsArbre(K_ARBRE_VOLUME_COMPLEXE, 8,
                      boule2arbre_bis(x, y, z, r, x1, yM, z1, xM, y, zM, prof+1),
                      boule2arbre_bis(x, y, z, r, xM, yM, z1, x2, y, zM, prof+1),
                      boule2arbre_bis(x, y, z, r, x1, y1, z1, xM, yM, zM, prof+1),
                      boule2arbre_bis(x, y, z, r, xM, y1, z1, x2, yM, zM, prof+1),
                      boule2arbre_bis(x, y, z, r, x1, yM, zM, xM, y2, z2, prof+1),
                      boule2arbre_bis(x, y, z, r, xM, yM, zM, x2, y2, z2, prof+1),
                      boule2arbre_bis(x, y, z, r, x1, y1, zM, xM, yM, zM, prof+1),
                      boule2arbre_bis(x, y, z, r, xM, y1, zM, x2, yM, z2, prof+1)
        );
}


karbre boule2arbre(int x, int y, int z, int r)
{
    return boule2arbre_bis(x, y, z, r, 0, 0, 0, N, N, N, 0);
}