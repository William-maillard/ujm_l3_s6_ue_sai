#ifndef _VOLUME_2_ARBRE_H_
#define _VOLUME_2_ARBRE_H_
#include <math.h>

#define N pow(3, 2) // 2^N taille arrete du cube initial

#define PROFONDEUR_MAX 5 // celle du k-arbre
#define K_ARBRE_VOLUME_PLEIN 1
#define K_ARBRE_VOLUME_VIDE 0
#define K_ARBRE_VOLUME_COMPLEXE 2

/*
typedef struct {
    int x, y, z, w;
}point;


/
 * p1 : sommet possedant les 3 coord les plsu petites.
 * p2 : sommet possedant les 3 coord les plus grandes
/
typedef struct {
    point p1, p2;
}cube;
*/

/* *** 
 * renvoie le 8-arbre representant le volume de la sphere 
 * de centre (x, y, z) et de rayon r.
 *** */
karbre boule2arbre(int x, int y, int z, int r);


karbre intersection(karbre V1, karbre V2) ;

#endif