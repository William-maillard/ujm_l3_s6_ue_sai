#include <stdio.h>
#include <stdlib.h>
#include "k-arbre.h"
#include "volume2arbre.h"

int main(int argc, char **argv) {

    int x, y, z, r;

    x = 4;
    y = 4;
    z = 4;
    r = 2;
    
    karbre a = boule2arbre(x, y, z, r);
    karbre b = boule2arbre(x/2, y/2, z*4, r);
    karbre c = intersection(a, b);    
    
    printf("8-arbre de la sphere de centre (%d, %d, %d) et de rayon %d\n",
           x, y, z, r);
    kAfficher(a);
        
    return 0;
}