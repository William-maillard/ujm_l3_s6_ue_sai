/* --------------
  |              |
  |     Ex 2     |
  |              |
   --------------*/
#ifndef _POLYGONE_H_
#define _POLYGONE_H_



typedef struct {
    int x, y;
} coordonnees;

typedef struct {
    coordonnees *sommets;
    int n; // le nb de sommets
} polygone;


typedef struct {
    coordonnees min, max; // par rapport aux y
} cote;

/*** Trace un polygone plein a n sommet avec le methode de balayage par ligne ***/
void remplir_polygone_convexe(polygone p);

#endif 