/**************************************************************
 *                                                            *
 * Implémente le remplissage d'un polygone 'ligne par ligne'  *
 *                                                            *
 **************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include "polygone.h"

/* --------------
  |              |
  |     Ex 2     |
  |              |
   --------------*/

/*** Trace un polygone plein a n sommet avec le methode de balayage par ligne ***/
void remplir_polygone_convexe(polygone p) {
    int *lcE, *lcS; /* liste candidat :
                       -Entrant : ordre des sommets a faire entrer. (ymin)
                       -Sortant : ordre des sommets a faire sortir. (ymax)
                       Les listes sont dans l'ordre croissant des y
                    */
    cote cote[p.n -1]; // liste des cotés du polygone
    int i, j, n;
    int y_min, y_max; // pour trier les listes lcE lcS
    int y0, x;
    int id_cote_min;
    double t;

    /* -- creation des cotes -- */
    n = p.n-1;
    for(i=0; i<p.n; i++) {
        if(p.sommets[i].y < p.sommets[i+1].y) {
            cote[i].min = p.sommets[i];
            cote[i].max = p.sommets[i+1];
        }
        else {
            cote[i].min = p.sommets[i+1];
            cote[i].max = p.sommets[i];
        }
    }
    // cas sommet n-1 -> 0
    if(p.sommets[n-1].y < p.sommets[0].y) {
        cote[i].min = p.sommets[n-1];
        cote[i].max = p.sommets[0];
    }
    else {
        cote[i].min = p.sommets[0];
        cote[i].max = p.sommets[n-1];
    }
    
    /* -- creation des listes -- */
    /* les listes sont de taille n, il y a n-1 cote donc l'indice 0
     contient un l'indice ou on en est dans la liste */
    lcE = (int *) malloc(p.n * sizeof(int));
    lcS = (int *) malloc(p.n * sizeof(int));
    if(lcE == NULL  ||  lcS == NULL) {
        fprintf(stderr, "pbm malloc !\n");
        exit(-1);
    }

    lcE[0] = 0;
    lcS[0] = 0;

    /* On fait un tri incremental en choissisant le plus petit (resp grand)
       a chaque tour de boucle */
    for(i = 0; i<p.n; i++) {

        y_min = cote[i].min.y;
        y_max = cote[i].max.y;
        
        for(j=i+1; j<p.n; j++) {

            if(cote[i].min.y < cote[y_min].min.y) {
                y_min = i;
            }
            if(cote[i].max.y > cote[y_max].max.y) {
                y_max = i;
            }         
        }

        lcE[i] = y_min;
        lcS[i] = y_max;
    }



    /* -- On remplis le polygone -- */
    y0 = cote[lcE[1]].min.y;
    lcE[0]++; // MAJ du compteur
    id_cote_min =1;

    while(lcS[0] < p.n) { /* i.e tant qu'il reste des sommets pas sortis,
                             donc des lignes a traiter */

        /* On cherche les intersections de la droite y=y0 avec les cotes du
           polygone compris entre id_cote_min et lcE[0] */
        for(i=id_cote_min; i<=lcE[0]; i++) {
            /*
              On cherche l'intersection entre P(i)P(i+1) (cote de p) et y=y0.
              On a : 
                |M(x,y) e P(i)P(i+1) <=> P(i)M = t P(i)P(i+1)
                |M(x,y) e y=y0 <=> y=y0
               
              On obtient le systeme :
                |x-x1 = t(x2-x1)
                |y-y1 = t(y2-y1)
                |y = y0
              ( avec P(i)(x1,y1) P(i+1)(x2, y2) )
              <=>
                |x = x1 + ((y0-y1)/(y2-y1)) (x2-x1)
                |t = (y0-y1) / (y2-y1)
                |y = y0

             Il faut verifier que te[0, 1] pour l'intersection se trouve sur
             le segement P(i)P(i+1)
            */

            // comment trouver l'intersection du prochain bord pour colorier
            // le segement ?
        }
    }
}