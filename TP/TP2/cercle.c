/***********************************************************
 *                                                         *
 * Fichier implémentant les fonctions de tracé de cercle.  *
 *                                                         *
 ***********************************************************/
#include <stdlib.h>
#include "cercle.h"
#include "pixel.h"

/* --------------
  |              |
  |     Ex 1     |
  |              |
   -------------- */

/*** Trace un cercle de rayon r ***/
void Bresenham_tracer_cercle_centre0(int r) {
    int x=0, y=r; // coordonnees du cercle.
    int d = 5 / 4 - r;

    while(y >= x) {
        trace_pixel(y, x);   // 1 octant
        trace_pixel(x, y);   // 2 octant
        trace_pixel(-x, y);  // 3 octant
        trace_pixel(-y, x);  // 4 octant
        trace_pixel(-y, -x); // 5 octant
        trace_pixel(-x, -y); // 6 octant
        trace_pixel(x, -y);  // 7 octant
        trace_pixel(y, -x);  // 8 octant

        // calcul du point suivant :
        if(d < 0) {
            d += 2 * x + 3;
        }
        else {
            d += 2*x -2*y + 5;
            y--;
        }
        x++;
    }
    
}

/*** Trace un cercle de rayon r et de centre (x0, y0) ***/
void Bresenham_tracer_cercle(int r, int x0, int y0) {
    int x=x0, y=y0 + r; // coordonnees pour le 2 octant.
    int d = 5 / 4 - (y0 + r);
    trace_pixel(x0, y0);
    
    while(y >= x) {
        trace_pixel(y, x);   // 1 octant
        trace_pixel(x, y);   // 2 octant
        trace_pixel(2*x0-x, y);  // 3 octant
        trace_pixel(2*x0-y, x);  // 4 octant
        trace_pixel(2*x0-y, 2*y0-x); // 5 octant
        trace_pixel(2*x0-x, 2*y0-y); // 6 octant
        trace_pixel(x, 2*y0-y);  // 7 octant
        trace_pixel(y, 2*y0-x);  // 8 octant

        // calcul du point suivant :
        if(d < 0) {
            d += 2 * x + 3;
        }
        else {
            d += 2*x -2*y + 5;
            y--;
        }
        x++;
    }
}