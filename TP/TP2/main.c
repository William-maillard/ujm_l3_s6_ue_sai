#include <stdio.h>
#include <stdlib.h>
#include "GL/gl.h" // OpenGL
#include "GL/glut.h" // glut pour fenetre graphique*
#include "cercle.h"
#include "polygone.h"

#define NB_PIX_X 68//544//272//136//68
#define NB_PIX_Y 34//272//136//68//34
#define TAILLE_PIXEL 0.5

/*** affiche un pixel aux cordonnees (x, y) sur la fenetre ***/
void trace_pixel(int x, int y) {
     glBegin(GL_QUADS);
     glVertex2f(x - TAILLE_PIXEL, y + TAILLE_PIXEL);
     glVertex2f(x + TAILLE_PIXEL, y + TAILLE_PIXEL);
     glVertex2f(x + TAILLE_PIXEL, y - TAILLE_PIXEL);
     glVertex2f(x - TAILLE_PIXEL, y - TAILLE_PIXEL);
     glEnd();
     
}


/*** fonction d'affichage de la fenetre graphique ***/
void Affichage() {    
    glClear(GL_COLOR_BUFFER_BIT); // remplis couleur par defaut
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-NB_PIX_X/2, NB_PIX_X/2, -NB_PIX_Y, NB_PIX_Y, 0, 1);
    /* projection orthogonal des points dans 
       xmin, xmax, ymin, ymax, zmin, zmax */
    
    /* -- Init matrice visualisation a la matrice identite -- */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //Bresenham_tracer_cercle_centre0(15);
    Bresenham_tracer_cercle(25, 5, 6);
    Bresenham_tracer_cercle(5, -2, -1);
    glFlush(); // envoie au buffer d'affichage
}


void usage(char *s) {
    printf("usage %s : num_exo <autre param>.\n", s);
    printf("si num exo = 1 : \n");


    exit(0);
}

//int exo;

int main(int argc, char **argv) {
    /* -- traitement des arguments du programme --
    if(argc < 2)
        usage(argv[0]);
    
    if(atoi(argv[1]) == 1)
        exo = 1;
    else
        exo = 2;
    */
    
    /* -- Initialisation de GLUT -- */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);

    /* -- Position et taille de la fenetre -- */
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    // glutFullScreen();

    /* -- Creation de la fenetre -- */
    glutCreateWindow("Une fenetre OpenGL");
    /* NB : retour un entier pour ientifier la fenetre */
    glutDisplayFunc(Affichage);

    glutMainLoop();    
    return 0;
}