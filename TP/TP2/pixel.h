#ifndef _PIXEL_H_
#define _PIXEL_H_

/*** affiche un pixel aux cordonnees (x, y) sur la fenetre ***/
void trace_pixel(int x, int y);

#endif