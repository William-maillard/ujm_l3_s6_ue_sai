/* --------------
  |              |
  |     Ex 1     |
  |              |
   -------------- */

/*** Trace un cercle de rayon r ***/
void Bresenham_tracer_cercle_centre0(int r);


/*** Trace un cercle de rayon r et de centre (x0, y0) ***/
void Bresenham_tracer_cercle(int r, int x0, int y0);