#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <windows.h>
#include <GL/glut.h>
#endif
#include<GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

#define NB_PIX_X 272//136//68
#define NB_PIX_Y 136//68//34
#define TAILLE_PIXEL 0.5


/*** affiche un pixel aux cordonnees (x, y) sur la fenetre ***/
void trace_pixel(int x, int y) {
    // depend des param de glOrtho

     glBegin(GL_QUADS);
     glVertex2f(x - TAILLE_PIXEL, y + TAILLE_PIXEL);
     glVertex2f(x + TAILLE_PIXEL, y + TAILLE_PIXEL);
     glVertex2f(x + TAILLE_PIXEL, y - TAILLE_PIXEL);
     glVertex2f(x - TAILLE_PIXEL, y - TAILLE_PIXEL);
     glEnd();

}

/*** trace le segmant (x1, y1) --> (x2, y2) ***/
void trace_segment_differentiel(int x1, int y1, int x2, int y2) {
    int x, y;
    double dx, dy;

    /* -- on part de l'equation parametrique --
    x(t) = x1 + t * (x2 - x1)
    y(t) = y1 + t *(y2 -y1)

    On prend la derive de chaque equation,
    pour calculer xi+1 en fonction de xi (resp yi) :
    xi+1 = xi + e * dx
    yi+1 = yi + e * dy

    e depend de la position des 2 points (i.e laquelle des
    coordonnees croit plus vite)
    */
    dx = x2 -x1;
    dy = y2 - y1;

    /* -- 2 cas : -- */
    if(dx > dy) { // l'increment sur les x doit etre de 1
        dy = dy / dx;
        dx = 1;
    }
    else{ // increment sur les y de 1
        dx = dx / dy;
        if(dy >= 0)
            dy = 1;
        else
            dy = -1;
    }

    /* -- debut du trace -- */
    x = x1; y = y1;
    while(x <= x2  &&  y <= y2) { /* NB : '<=' et pas '!=' car
                                   avec les arrondis des /
                                   on ne va pas tomber pile.
                                 */
        trace_pixel(x, y);

        // on passe au point suivant :
        x += dx;
        y += dy;
    }

}


/* ----- Dans le 1° octant ------ */

void Bresenham_initial(int x1, int y1, int x2, int y2) {
  int x, y = y1;
  float erreur = -0.5;
  float m = (float) (y2 - y1) / (float) (x2 - x1);

  for(x= x1; x<=x2; x++) {
    trace_pixel(x, y);
    erreur += m;
    if(erreur >= 0) {
      y++;
      erreur--;
    }
  }
}


void Bresenham_octant1(int x1, int y1, int x2, int y2, int octant) {
    int x, y = y1;
    int dx = x2 -x1, dy = y2 - y1;
    int m = 2 * dy;
    int erreur = dx;

    for(x= x1; x <= x2; x++) {
        switch(octant) {
        case 1 :
            trace_pixel(x, y);
            break;
        case 2:
            trace_pixel(y, x);
            break;
        case 3 :
            trace_pixel(x1 - y, x);
            break;
        case 4:
            trace_pixel(x1 - x, y);
            break;
        case 5 :
            trace_pixel(x1 - y, y1 - x);
            break;
        case 6:
            trace_pixel(x1 - x, y1 - y);
            break;
        case 7 :
            trace_pixel(y, y1 - x);
            break;
        default:
            trace_pixel(x, y1 - y);
        }
        erreur += 2 * dy;
        if(erreur >= 0) {
            y++;
            erreur -= m;
        }
    }
}

void Bresenham(int x1, int y1, int x2, int y2) {
  int x, y = y1;
  int dx = x2 -x1, dy = y2 - y1;
  int erreur;
  int octant;

  /* -- on cherche dans quel octant on se trouve -- */
  // on coupe le cercle horizontalement
  if(y1 < y2) // demi-cercle haut
    octant = 1;
  else // bas
    octant = 5;

  // on le coupe verticalement
  if(x1 > x2)
    octant += 2;

  // on coupe la diagonale
  if(dy > dx)
    octant++;

  /* -- on trace le segment -- */
  switch(octant) {
  case 1 :
      Bresenham_octant1(x1, y1, x2, y2, 1);
      break;
  case 2: // symétrie par rapport a la diagonale
      Bresenham_octant1(x1, y1, y2, x2, 2);
      break;
  case 3 : // symetrie par rapport a la droite verticale + la diagonale
      Bresenham_octant1(x1, y1, y2, x1 + (x1 - x2), 3);
      break;
  case 4: // symetrie verticale
      Bresenham_octant1(x1, y1, x1 + (x1 - x2), y2, 4);
      break;
  case 5 : // verticale, horizontale et diagonale
      Bresenham_octant1(x1, y1, y1 + (y1 - y2), x1 + (x1 - x2), 5);
      break;
  case 6: // verticale et horizontale
      Bresenham_octant1(x1, y1, x1 + (x1 - x2), y1 + (y1 - y2), 6);
      break;
  case 7 : // horizontale et diagonale
      Bresenham_octant1(x1, y1, y1 + (y1 - y2), x2 , 7);
      break;
  default: // horizontale
      Bresenham_octant1(x1, y1, x2, y1 + (y1 - y2) , 8);
  }
}


/*** fonction d'affichage de la fenetre graphique ***/
void Affichage() {
    glClear(GL_COLOR_BUFFER_BIT); // remplis couleur par defaut
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-NB_PIX_X/2, NB_PIX_X/2, -NB_PIX_Y, NB_PIX_Y, 0, 1); /* projection orthogonal des points dans
                                    xmin, xmax, ymin, ymax, zmin, zmax */

    /* -- Init matrice visualisation a la matrice identite -- */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /* -- definition des points a tracer --
    glBegin(GL_POINTS);
    glVertex2i(0, 1);
    glVertex2i(-1, 0);
    glVertex2i(1,0);
    glEnd();

    glBegin(GL_LINES);
    glVertex2i(5, 0);
    glVertex2i(-5, 0);
    glVertex2i(0, -5);
    glVertex2i(0, 5);
    glEnd();
    */
    //trace_pixel(1, 1);
    //race_pixel(2, 2);
    //trace_pixel(5,5);

    /* test des pixels qui se touchent sur les bords.
    int n = NB_PIX_X/2;
    for(int i =0; i<n; i++) {
        trace_pixel(i,i);
    }
    */
    // un pixel :
    trace_pixel(0,0);
    /*
    // droite horizontale :
    trace_segment_differentiel(2, 0, 6, 0);
    // droite verticale :
    trace_segment_differentiel(0, 2, 0, 6);
    // droite diagonale NE :
    trace_segment_differentiel(2, 2, 6, 6);
    */
    //Bresenham_initial(0, 0, 6, 6);
    Bresenham(0, 0, 6, 5); // 1 octant
    Bresenham(0, 0, 6, 10); // 2 octant
    Bresenham(0, 0, -6, 2);// 4 octant
    Bresenham(0, 0, 4, -3); // 8 octant
    Bresenham(0, 0, 4, -10); // 7 octant
    glFlush(); // envoie au buffer d'affichage
}



int main(int argc, char **argv) {

    /* -- Initialisation de GLUT -- */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);

    /* -- Position et taille de la fenetre -- */
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    // glutFullScreen();

    /* -- Creation de la fenetre -- */
    glutCreateWindow("Une fenetre OpenGL"); /* NB : retour un entier pour
                                               identifier la fenetre */
    glutDisplayFunc(Affichage);

    trace_pixel(0, 1);


    glutMainLoop();
    return 0;
}
